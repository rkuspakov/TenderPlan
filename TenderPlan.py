import os
import zipfile
import xmltodict, json
from pathlib import Path
import sqlite3
import datetime


# подключение к базе данных
conn = sqlite3.connect("TenderPlan.db")
cursor = conn.cursor()


def save_json(dir_name, file_name, json_file):
    file_name = os.path.join(dir_name + 'TenderPlan', file_name.replace("xml", "json"))
    with open(os.path.join(file_name), 'w+') as f:
        f.write(json.dumps(json_file))


def add_commonInfoPlanPeriod(id, txt):
    try:
        currenttxt = txt
        firstYear = ''
        secondYear = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:firstYear':
                firstYear = v
            elif k == 'ns5:secondYear':
                secondYear = v
            else:
                print(k + ' - нет if')

            # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_commonInfoplanPeriod WHERE id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_commonInfoplanPeriod (id, firstYear, secondYear) VALUES(?,?,?)""",
                (id, firstYear, secondYear))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_commonInfoplanPeriod SET firstYear = ?, secondYear = ? WHERE id = ?""",
                (firstYear, secondYear, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_commonInfoPlancustomerInfo(id, txt):
    try:
        currenttxt = txt
        regNum = ''
        fullName = ''
        INN = ''
        KPP = ''
        OKTMO = ''
        factAddress = ''
        phone = ''
        email = ''
        OKOPF = ''
        OKFS = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:regNum':
                regNum = v
            elif k == 'ns5:fullName':
                fullName = v
            elif k == 'ns5:INN':
                INN = v
            elif k == 'ns5:KPP':
                KPP = v
            elif k == 'ns5:OKTMO':
                OKTMO = v['ns2:code']
            elif k == 'ns5:factAddress':
                factAddress = v
            elif k == 'ns5:phone':
                phone = v
            elif k == 'ns5:email':
                email = v
            elif k == 'ns5:OKOPF':
                OKOPF = v['ns2:code']
            elif k == 'ns5:OKFS':
                OKFS = v['ns2:code']
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_commonInfocustomerInfo WHERE id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_commonInfocustomerInfo (id, regNum, fullName, INN, KPP, OKTMO, factAddress, phone, email, OKOPF, OKFS) VALUES(?,?,?,?,?,?,?,?,?,?,?)""",
                (id, regNum, fullName, INN, KPP, OKTMO, factAddress, phone, email, OKOPF, OKFS))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_commonInfocustomerInfo SET regNum = ?, fullName = ?, INN = ?, KPP = ?, OKTMO = ?, factAddress = ?, phone = ?, email = ?, OKOPF = ?, OKFS = ? WHERE id = ?""",
                (regNum, fullName, INN, KPP, OKTMO, factAddress, phone, email, OKOPF, OKFS, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_commonInfoPlanConfirmContactInfo(id, txt):
    try:
        currenttxt = txt
        lastName = ''
        firstName = ''
        middleName = ''
        position = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:lastName':
                lastName = v
            elif k == 'ns5:firstName':
                firstName = v
            elif k == 'ns5:middleName':
                middleName = v
            elif k == 'ns5:position':
                position = v
            else:
                print(k + ' - нет if')

            # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_commonInfoconfirmContactInfo WHERE id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_commonInfoconfirmContactInfo (id, lastName, firstName, middleName, position) VALUES(?,?,?,?,?)""",
                (id, lastName, firstName, middleName, position))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_commonInfoconfirmContactInfo SET lastName = ?, firstName = ?, middleName = ?, position = ? WHERE id = ?""",
                (lastName, firstName, middleName, position, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_commonInfoPlanownerInfo(id, txt):
    try:
        currenttxt = txt
        regNum = ''
        fullName = ''
        INN = ''
        KPP = ''
        OKTMO = ''
        factAddress = ''
        phone = ''
        email = ''
        ownerRole = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:regNum':
                regNum = v
            elif k == 'ns5:fullName':
                fullName = v
            elif k == 'ns5:INN':
                INN = v
            elif k == 'ns5:KPP':
                KPP = v
            elif k == 'ns5:OKTMO':
                OKTMO = v['ns2:code']
            elif k == 'ns5:factAddress':
                factAddress = v
            elif k == 'ns5:phone':
                phone = v
            elif k == 'ns5:email':
                email = v
            elif k == 'ns5:ownerRole':
                ownerRole = v
            else:
                print(k + ' - нет if1')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_commonInfoownerInfo WHERE id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_commonInfoownerInfo (id, regNum, fullName, INN, KPP, OKTMO, factAddress, phone, email, ownerRole) VALUES(?,?,?,?,?,?,?,?,?,?)""",
                (id, regNum, fullName, INN, KPP, OKTMO, factAddress, phone, email, ownerRole))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_commonInfoownerInfo SET regNum = ?, fullName = ?, INN = ?, KPP = ?, OKTMO = ?, factAddress = ?, phone = ?, email = ?, ownerRole = ? WHERE id = ?""",
                (regNum, fullName, INN, KPP, OKTMO, factAddress, phone, email, ownerRole, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_commonInfo(id, txt):
    try:
        currenttxt = txt
        planYear = ''
        createDate = ''
        confirmDate = ''
        directDate = ''
        publishDate = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:planYear':
                planYear = v
            elif k == 'ns5:createDate':
                createDate = v
            elif k == 'ns5:confirmDate':
                confirmDate = v
            elif k == 'ns5:directDate':
                directDate = v
            elif k == 'ns5:publishDate':
                publishDate = v
            elif k == 'ns5:planPeriod':
                add_commonInfoPlanPeriod(id, v)
            elif k == 'ns5:customerInfo':
                add_commonInfoPlancustomerInfo(id, v)
            elif k == 'ns5:confirmContactInfo':
                add_commonInfoPlanConfirmContactInfo(id, v)
            elif k == 'ns5:ownerInfo':
                add_commonInfoPlanownerInfo(id, v)
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_commonInfo WHERE id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_commonInfo (id, planYear, createDate, confirmDate, directDate, publishDate) VALUES(?,?,?,?,?,?)""",
                (id, planYear, createDate, confirmDate, directDate, publishDate))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_commonInfo SET planYear = ?, createDate = ?, confirmDate = ?, directDate = ?, publishDate = ? WHERE id = ?""",
                (planYear, createDate, confirmDate, directDate, publishDate, id))
            conn.commit()

    except KeyError as e:
        print("Отсутствует значение", e)


def add_PositionsKVR(id, extNumber, txt):
    try:
        kvr = ''
        total = ''
        currentYear = ''
        firstYear = ''
        secondYear = ''
        subsecYears = ''
        if isinstance(txt, dict):
            currenttxt = txt
            for (k, v) in currenttxt.items():
                if k == 'ns5:KVRYearsInfo':
                    total = v['ns5:total']
                    currentYear = v['ns5:currentYear']
                    firstYear = v['ns5:firstYear']
                    secondYear = v['ns5:secondYear']
                    subsecYears = v['ns5:subsecYears']
                elif k == 'ns5:KVR':
                    kvr = v['ns2:code']
                else:
                    print(k + ' - нет if')
            InsertPositionsKVR(id, extNumber, kvr, total, currentYear, firstYear, secondYear, subsecYears)
        else:
            for currenttxt in txt:
                for (k, v) in currenttxt.items():
                    if k == 'ns5:KVRYearsInfo':
                        total = v['ns5:total']
                        currentYear = v['ns5:currentYear']
                        firstYear = v['ns5:firstYear']
                        secondYear = v['ns5:secondYear']
                        subsecYears = v['ns5:subsecYears']
                    elif k == 'ns5:KVR':
                        kvr = v['ns2:code']
                    else:
                        print(k + ' - нет if')
                InsertPositionsKVR(id, extNumber, kvr, total, currentYear, firstYear, secondYear, subsecYears)

    except KeyError as e:
        print("Отсутствует значение", e)


def InsertPositionsKVR(id, extNumber, kvr, total, currentYear, firstYear, secondYear, subsecYears):
    # проверка наличия записи в базе
    cursor.execute(
        "SELECT * FROM tenderPlan_PositionsKVR WHERE id = '" + id + "' and kvr = '" + kvr + "' and extNumber = '" + extNumber + "';")
    one_result = cursor.fetchone()

    # запись в базу данных
    if one_result is None:
        cursor.execute(
            """INSERT INTO tenderPlan_PositionsKVR (id, kvr, extNumber, total, currentYear, firstYear, secondYear, subsecYears) VALUES(?,?,?,?,?,?,?,?)""",
            (id, kvr, extNumber, total, currentYear, firstYear, secondYear, subsecYears))
        conn.commit()
    else:
        cursor.execute(
            """UPDATE tenderPlan_PositionsKVR SET total = ?, currentYear = ?, firstYear = ?, secondYear = ?, subsecYears = ? WHERE id = ? and kvr = ? and extNumber = ?""",
            (total, currentYear, firstYear, secondYear, subsecYears, id, extNumber, kvr))
        conn.commit()


def add_PositionsOKPD2(id, extNumber, txt):
    try:
        code = ''
        name = ''
        if isinstance(txt, dict):
            currenttxt = txt
            for (k, v) in currenttxt.items():
                if k == 'ns2:OKPDCode':
                    code = v
                elif k == 'ns2:OKPDName':
                    name = v
                else:
                    print(k + ' - нет if')
            InsertPositionsOKPD2(id, extNumber, code, name)
        else:
            for currenttxt in txt:
                for (k, v) in currenttxt.items():
                    if k == 'ns2:OKPDCode':
                        code = v
                    elif k == 'ns2:OKPDName':
                        name = v
                    else:
                        print(k + ' - нет if')
            InsertPositionsOKPD2(id, extNumber, code, name)

    except KeyError as e:
        print("Отсутствует значение", e)


def InsertPositionsOKPD2(id, extNumber, code, name):
    # проверка наличия записи в базе
    cursor.execute(
        "SELECT * FROM tenderPlan_PositionsOKPD2 WHERE id = '" + id + "' and extNumber = '" + extNumber + "' and code = '" + code + "';")
    one_result = cursor.fetchone()

    # запись в базу данных
    if one_result is None:
        cursor.execute(
            """INSERT INTO tenderPlan_PositionsOKPD2 (id, extNumber, code, name) VALUES(?,?,?,?)""",
            (id, extNumber, code, name))
        conn.commit()
    else:
        cursor.execute(
            """UPDATE tenderPlan_PositionsOKPD2 SET name = ? WHERE id = ? and extNumber = ? and code = ?""",
            (code, name, id, extNumber,))
        conn.commit()


def add_PositionsfinceInfo(id, extNumber, txt):
    try:
        currenttxt = txt
        total = ''
        currentYear = ''
        firstYear = ''
        secondYear = ''
        subsecYears = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:total':
                total = v
            elif k == 'ns5:currentYear':
                currentYear = v
            elif k == 'ns5:firstYear':
                firstYear = v
            elif k == 'ns5:secondYear':
                secondYear = v
            elif k == 'ns5:subsecYears':
                subsecYears = v
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_PositionsfinanceInfo WHERE id = '" + id + "' and extNumber = '" + extNumber + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_PositionsfinanceInfo (id, extNumber, total, currentYear, firstYear, secondYear, subsecYears) VALUES(?,?,?,?,?,?,?)""",
                (id, extNumber, total, currentYear, firstYear, secondYear, subsecYears))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_PositionsfinanceInfo SET total = ?, currentYear = ?, firstYear = ?, secondYear = ?, subsecYears = ? WHERE id = ? and extNumber = ?""",
                (total, currentYear, firstYear, secondYear, subsecYears, id, extNumber))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_positions(id, txt):
    try:
        positionNumber = ''
        extNumber = ''
        IKZ = ''
        publishYear = ''
        IKU = ''
        purchaseNumber = ''
        purchaseObjectInfo = ''
        publicDiscussion = ''
        positionCanceled = ''
        isEnergyServiceContract = ''

        for currenttxt in txt:
            for (k, v) in currenttxt.items():
                if k == 'ns5:commonInfo':
                    positionNumber = v['ns5:positionNumber']
                    extNumber = v['ns5:extNumber']
                    IKZ = v['ns5:IKZ']
                    publishYear = v['ns5:publishYear']
                    IKU = v['ns5:IKU']
                    purchaseNumber = v['ns5:purchaseNumber']
                    add_PositionsOKPD2(id, extNumber, v['ns5:undefined']['ns5:OKPD2s']['ns5:OKPD2Info'])
                    purchaseObjectInfo = v['ns5:purchaseObjectInfo']
                    publicDiscussion = v['ns5:publicDiscussion']
                    positionCanceled = v['ns5:positionCanceled']
                elif k == 'ns5:isEnergyServiceContract':
                    isEnergyServiceContract = v
                elif k == 'ns5:financeInfo':
                    add_PositionsfinceInfo(id, extNumber, v)
                elif k == 'ns5:KVRsInfo':
                    add_PositionsKVR(id, extNumber, v['ns5:KVRInfo'])
                else:
                    print(k + ' - нет if')

            # проверка наличия записи в базе
            cursor.execute(
                "SELECT * FROM tenderPlan_Positions WHERE id = '" + id + "' and extNumber = '" + extNumber + "';")
            one_result = cursor.fetchone()

            # запись в базу данных
            if one_result is None:
                cursor.execute(
                    """INSERT INTO tenderPlan_Positions (id, positionNumber, extNumber, IKZ, publishYear, IKU, purchaseNumber, purchaseObjectInfo, publicDiscussion, positionCanceled, isEnergyServiceContract) VALUES(?,?,?,?,?,?,?,?,?,?,?)""",
                    (id, positionNumber, extNumber, IKZ, publishYear, IKU, purchaseNumber,
                        purchaseObjectInfo,
                        publicDiscussion, positionCanceled, isEnergyServiceContract))
                conn.commit()
            else:
                cursor.execute(
                    """UPDATE tenderPlan_Positions SET positionNumber = ?, IKZ = ?, publishYear = ?, IKU = ?, purchaseNumber = ?, purchaseObjectInfo = ?, publicDiscussion = ?, positionCanceled = ?, isEnergyServiceContract = ? WHERE id = ? and extNumber = ?""",
                    (positionNumber, IKZ, publishYear, IKU, purchaseNumber, purchaseObjectInfo,
                     publicDiscussion, positionCanceled, isEnergyServiceContract, id, extNumber))
                conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_specialPurchasePositionsfinanceInfo(id, txt):
    try:
        currenttxt = txt
        total = ''
        currentYear = ''
        firstYear = ''
        secondYear = ''
        subsecYears = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:total':
                total = v
            elif k == 'ns5:currentYear':
                currentYear = v
            elif k == 'ns5:firstYear':
                firstYear = v
            elif k == 'ns5:secondYear':
                secondYear = v
            elif k == 'ns5:subsecYears':
                subsecYears = v
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_specialPurchasePositionsfinanceInfo WHERE id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_specialPurchasePositionsfinanceInfo (id, total, currentYear, firstYear, secondYear, subsecYears) VALUES(?,?,?,?,?,?)""",
                (id, total, currentYear, firstYear, secondYear, subsecYears))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_specialPurchasePositionsfinanceInfo SET total = ?, currentYear = ?, firstYear = ?, secondYear = ?, subsecYears = ? WHERE id = ?""",
                (total, currentYear, firstYear, secondYear, subsecYears, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_specialPurchasePositionsModification(id, txt):
    try:
        currenttxt = txt
        changeReason = ''
        decision = ''
        description = ''
        modificationNumber = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:changeReason':
                changeReason = v['ns5:code']
            elif k == 'ns5:decision':
                decision = v['ns5:purchaseChanged']
            elif k == 'ns5:description':
                description = v
            elif k == 'ns5:modificationNumber':
                modificationNumber = v
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_specialPurchasePositionsModification WHERE id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_specialPurchasePositionsModification (id, changeReason, decision, description, modificationNumber) VALUES(?,?,?,?,?)""",
                (id, changeReason, decision, description, modificationNumber))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_specialPurchasePositionsModification SET changeReason = ?, decision = ?, description = ?, modificationNumber = ? WHERE id = ?""",
                (changeReason, decision, description, modificationNumber, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_specialPurchasePositionsKVR(id, txt):
    try:
        currenttxt = txt
        kvr = ''
        total = ''
        currentYear = ''
        firstYear = ''
        secondYear = ''
        subsecYears = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:KVRYearsInfo':
                total = v['ns5:total']
                currentYear = v['ns5:currentYear']
                firstYear = v['ns5:firstYear']
                secondYear = v['ns5:secondYear']
                subsecYears = v['ns5:subsecYears']
            elif k == 'ns5:KVR':
                kvr = v['ns2:code']

            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_specialPurchasePositionsKVR WHERE id = '" + id + "' and kvr = '" + kvr + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_specialPurchasePositionsKVR (id, kvr, total, currentYear, firstYear, secondYear, subsecYears) VALUES(?,?,?,?,?,?,?)""",
                (id, kvr, total, currentYear, firstYear, secondYear, subsecYears))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_specialPurchasePositionsKVR SET kvr = ?, total = ?, currentYear = ?, firstYear = ?, secondYear = ?, subsecYears = ? WHERE id = ?""",
                (kvr, total, currentYear, firstYear, secondYear, subsecYears, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_specialPurchasePositions(id, txt):
    try:
        currenttxt = txt
        type = ''
        positionNumber = ''
        extNumber = ''
        IKZ = ''
        publishYear = ''
        IKU = ''
        kvr = ''
        purchaseNumber = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:specialPurchasePosition':
                type = v['ns5:type']['ns5:code']
                positionNumber = v['ns5:positionNumber']
                extNumber = v['ns5:extNumber']
                IKZ = v['ns5:IKZ']
                publishYear = v['ns5:publishYear']
                IKU = v['ns5:IKU']
                kvr = v['ns5:KVRInfo']['ns5:KVR']['ns2:code']
                purchaseNumber = v['ns5:purchaseNumber']
                add_specialPurchasePositionsKVR(id, v['ns5:KVRsInfo']['ns5:KVRInfo'])
                add_specialPurchasePositionsModification(id, v['ns5:specialPurchasePositionModification'])
                add_specialPurchasePositionsfinanceInfo(id, v['ns5:financeInfo'])
            else:
                print(k + ' - нет if')

            # проверка наличия записи в базе
            cursor.execute(
                "SELECT * FROM tenderPlan_specialPurchasePositions WHERE id = '" + id + "';")
            one_result = cursor.fetchone()

            # запись в базу данных
            if one_result is None:
                cursor.execute(
                    """INSERT INTO tenderPlan_specialPurchasePositions (id, type, positionNumber, extNumber, IKZ, publishYear, IKU, kvr, purchaseNumber) VALUES(?,?,?,?,?,?,?,?,?)""",
                    (id, type, positionNumber, extNumber, IKZ, publishYear, IKU, kvr, purchaseNumber))
                conn.commit()
            else:
                cursor.execute(
                    """UPDATE tenderPlan_specialPurchasePositions SET type = ?, positionNumber = ?, extNumber = ?,IKZ = ?, publishYear = ?, IKU = ?, kvr = ?, purchaseNumber = ? WHERE id = ?""",
                    (type, positionNumber, extNumber, IKZ, publishYear, IKU, kvr, purchaseNumber, id))
                conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_outcomeIndicatorsKVR(id, txt):
    try:
        kvr = ''
        total = ''
        currentYear = ''
        firstYear = ''
        secondYear = ''
        subsecYears = ''

        for currenttxt in txt:
            for (k, v) in currenttxt.items():
                if k == 'ns5:KVRTotalsYearsInfo':
                    total = v['ns5:total']
                    currentYear = v['ns5:currentYear']
                    firstYear = v['ns5:firstYear']
                    secondYear = v['ns5:secondYear']
                    subsecYears = v['ns5:subsecYears']
                elif k == 'ns5:KVR':
                    kvr = v['ns2:code']
                else:
                    print(k + ' - нет if')

            # проверка наличия записи в базе
            cursor.execute(
                "SELECT * FROM tenderPlan_outcomeIndicators WHERE id = '" + id + "' and kvr = '" + kvr + "';")
            one_result = cursor.fetchone()

            # запись в базу данных
            if one_result is None:
                cursor.execute(
                    """INSERT INTO tenderPlan_outcomeIndicators (id, kvr, total, currentYear, firstYear, secondYear, subsecYears) VALUES(?,?,?,?,?,?,?)""",
                    (id, kvr, total, currentYear, firstYear, secondYear, subsecYears))
                conn.commit()
            else:
                cursor.execute(
                    """UPDATE tenderPlan_outcomeIndicators SET kvr = ?, total = ?, currentYear = ?, firstYear = ?, secondYear = ?, subsecYears = ? WHERE id = ?""",
                    (kvr, total, currentYear, firstYear, secondYear, subsecYears, id))
                conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_outcomeIndicators(id, txt):
    try:
        currenttxt = txt
        kvr = ''
        total = ''
        currentYear = ''
        firstYear = ''
        secondYear = ''
        subsecYears = ''

        for (k, v) in currenttxt.items():
            if k == 'ns5:total':
                total = v
            elif k == 'ns5:currentYear':
                currentYear = v
            elif k == 'ns5:firstYear':
                firstYear = v
            elif k == 'ns5:secondYear':
                secondYear = v
            elif k == 'ns5:subsecYears':
                subsecYears = v
            elif k == 'ns5:KVR':
                kvr = v['ns2:code']
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_outcomeIndicators WHERE id = '" + id + "' and kvr = '" + kvr + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute(
                """INSERT INTO tenderPlan_outcomeIndicators (id, kvr, total, currentYear, firstYear, secondYear, subsecYears) VALUES(?,?,?,?,?,?,?)""",
                (id, kvr, total, currentYear, firstYear, secondYear, subsecYears))
            conn.commit()
        else:
            cursor.execute(
                """UPDATE tenderPlan_outcomeIndicators SET total = ?, currentYear = ?, firstYear = ?, secondYear = ?, subsecYears = ? WHERE id = ? and kvr = ?""",
                (total, currentYear, firstYear, secondYear, subsecYears, id, kvr))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_attachments(id, txt):
    try:
        currenttxt = txt
        for (k, v) in currenttxt.items():
            if k == 'ns4:attachmentInfo':
                notPublishedOnEIS = v['ns4:notPublishedOnEIS']
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_attachments WHERE tenderPlan_attachments.id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute("""INSERT INTO tenderPlan_attachments (id, notPublishedOnEIS) VALUES(?,?)""",
                           (id, notPublishedOnEIS))
            conn.commit()
        else:
            cursor.execute("""UPDATE tenderPlan_attachments SET notPublishedOnEIS = ? WHERE id = ?""",
                           (notPublishedOnEIS, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def add_printForm(id, txt):
    try:
        currenttxt = txt
        for (k, v) in currenttxt.items():
            if k == 'ns4:url':
                url = v
            elif k == 'ns4:signature':
                signature = v['#text']
            else:
                print(k + ' - нет if')

        # проверка наличия записи в базе
        cursor.execute(
            "SELECT * FROM tenderPlan_printForm WHERE tenderPlan_printForm.id = '" + id + "';")
        one_result = cursor.fetchone()

        # запись в базу данных
        if one_result is None:
            cursor.execute("""INSERT INTO tenderPlan_printForm (id, url, signature) VALUES(?,?,?)""",
                           (id, url, signature))
            conn.commit()
        else:
            cursor.execute("""UPDATE tenderPlan_printForm SET url = ?, signature = ? WHERE id = ?""",
                           (url, signature, id))
            conn.commit()
    except KeyError as e:
        print("Отсутствует значение", e)


def insert_tender_Plan(F):
    # проверка наличия записи в базе
    cursor.execute(
        "SELECT * FROM TenderPlan WHERE TenderPlan.id = '" + F['id'] + "';")
    one_result = cursor.fetchone()

    # запись в базу данных
    if one_result is None:
        cursor.execute("""INSERT INTO TenderPlan (id, externalId, planNumber, versionNumber) VALUES(?,?,?,?)""", (
            F['id'], F['id'], F['id'], F['id']
        ))
        conn.commit()
    else:
        cursor.execute("""UPDATE TenderPlan SET externalId = ?, planNumber = ?, versionNumber = ?
                          WHERE id = ? """, (F['id'], F['id'], F['id'], F['id']))
        conn.commit()


def get_tender_Plan(json_file):
    F = {}
    id = None
    externalId = None
    planNumber = None
    versionNumber = None

    currenttxt = json_file['ns3:export']['ns3:tenderPlan2020']
    for (k, v) in currenttxt.items():
        try:
            if k == 'ns5:id':
                id = v
            if k == 'ns5:externalId':
                externalId = v
            if k == 'ns5:planNumber':
                planNumber = v
            if k == 'ns5:versionNumber':
                versionNumber = v
            if k == 'ns5:commonInfo':
                add_commonInfo(id, currenttxt['ns5:commonInfo'])
            if k == 'ns5:positions':
                add_positions(id, currenttxt['ns5:positions']['ns5:position'])
            if k == 'ns5:specialPurchasePositions':
                add_specialPurchasePositions(id, currenttxt['ns5:specialPurchasePositions'])
            if k == 'ns5:outcomeIndicators':
                add_outcomeIndicators(id, currenttxt['ns5:outcomeIndicators']['ns5:totalsInfo'])
                add_outcomeIndicatorsKVR(id,
                                         currenttxt['ns5:outcomeIndicators']['ns5:KVRsTotalsInfo']['ns5:KVRTotalsInfo'])
            if k == 'ns5:attachments':
                add_attachments(id, currenttxt['ns5:attachments'])
            if k == 'ns5:printForm':
                add_printForm(id, currenttxt['ns5:printForm'])
        except KeyError as e:
            print("Отсутствует значение", e)

    F['id'] = id
    F['idexternalId'] = externalId
    F['planNumber'] = planNumber
    F['versionNumber'] = versionNumber

    return F


def read_tender_Plan(dir_name):
    all_files = os.listdir(dir_name)
    for files in all_files:
        fullname = os.path.join(dir_name, files)
        if Path(fullname).suffix == '.zip':
            with zipfile.ZipFile(fullname, mode="r") as fp:
                for f in fp.filelist:
                    xml_file = fp.read(f.filename).decode(encoding="utf-8")
                    json_file = xmltodict.parse(xml_file)
                    F = get_tender_Plan(json_file)
                    insert_tender_Plan(F)
        else:
            with open(fullname, 'r', encoding='utf-8') as f:
                xml_file = f.read()
            json_file = xmltodict.parse(xml_file)
            F = get_tender_Plan(json_file)
            insert_tender_Plan(F)


if __name__ == '__main__':
    dir_name = r"download"
    print(datetime.datetime.now())
    read_tender_Plan(dir_name)
    print(datetime.datetime.now())
