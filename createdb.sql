--
-- Файл сгенерирован с помощью SQLiteStudio v3.3.0 в Вт июн 4 18:13:27 2024
--
-- Использованная кодировка текста: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: tenderPlan
DROP TABLE IF EXISTS tenderPlan;
CREATE TABLE tenderPlan (id INTEGER, externalId INTEGER, planNumber TEXT, versionNumber INTEGER);

-- Таблица: tenderPlan_attachments
DROP TABLE IF EXISTS tenderPlan_attachments;
CREATE TABLE tenderPlan_attachments (id INTEGER, notPublishedOnEIS BOOLEAN);

-- Таблица: tenderPlan_commonInfo
DROP TABLE IF EXISTS tenderPlan_commonInfo;
CREATE TABLE tenderPlan_commonInfo (id INTEGER, planYear INTEGER, createDate DATETIME, confirmDate DATETIME, directDate DATETIME, publishDate DATETIME);

-- Таблица: tenderPlan_commonInfoconfirmContactInfo
DROP TABLE IF EXISTS tenderPlan_commonInfoconfirmContactInfo;
CREATE TABLE tenderPlan_commonInfoconfirmContactInfo (id INTEGER, lastName TEXT, firstName TEXT, middleName TEXT, position TEXT);

-- Таблица: tenderPlan_commonInfocustomerInfo
DROP TABLE IF EXISTS tenderPlan_commonInfocustomerInfo;
CREATE TABLE tenderPlan_commonInfocustomerInfo (id INTEGER, regNum TEXT, fullName TEXT, INN TEXT, KPP TEXT, OKTMO TEXT, factAddress TEXT, phone TEXT, email TEXT, OKOPF INTEGER, OKFS INTEGER);

-- Таблица: tenderPlan_commonInfoownerInfo
DROP TABLE IF EXISTS tenderPlan_commonInfoownerInfo;
CREATE TABLE tenderPlan_commonInfoownerInfo (id INTEGER, regNum TEXT, fullName TEXT, INN TEXT, KPP TEXT, OKTMO TEXT, factAddress TEXT, phone TEXT, email TEXT, ownerRole TEXT);

-- Таблица: tenderPlan_commonInfoplanPeriod
DROP TABLE IF EXISTS tenderPlan_commonInfoplanPeriod;
CREATE TABLE tenderPlan_commonInfoplanPeriod (id INTEGER, firstYear INTEGER, secondYear INTEGER);

-- Таблица: tenderPlan_outcomeIndicators
DROP TABLE IF EXISTS tenderPlan_outcomeIndicators;
CREATE TABLE tenderPlan_outcomeIndicators (id INTEGER, kvr INTEGER, total DOUBLE, currentYear DOUBLE, firstYear DOUBLE, secondYear DOUBLE, subsecYears DOUBLE);

-- Таблица: tenderPlan_Positions
DROP TABLE IF EXISTS tenderPlan_Positions;
CREATE TABLE tenderPlan_Positions (id INTEGER, positionNumber TEXT, extNumber TEXT, IKZ TEXT, publishYear INTEGER, IKU TEXT, purchaseNumber TEXT, purchaseObjectInfo TEXT, publicDiscussion BOOLEAN, positionCanceled BOOLEAN, isEnergyServiceContract BOOLEAN);

-- Таблица: tenderPlan_PositionsfinanceInfo
DROP TABLE IF EXISTS tenderPlan_PositionsfinanceInfo;
CREATE TABLE tenderPlan_PositionsfinanceInfo (id INTEGER, extNumber TEXT, total DOUBLE, currentYear DOUBLE, firstYear DOUBLE, secondYear DOUBLE, subsecYears DOUBLE);

-- Таблица: tenderPlan_PositionsKVR
DROP TABLE IF EXISTS tenderPlan_PositionsKVR;
CREATE TABLE tenderPlan_PositionsKVR (id INTEGER, kvr INTEGER, extNumber TEXT, total DOUBLE, currentYear DOUBLE, firstYear DOUBLE, secondYear DOUBLE, subsecYears DOUBLE);

-- Таблица: tenderPlan_PositionsOKPD2
DROP TABLE IF EXISTS tenderPlan_PositionsOKPD2;
CREATE TABLE tenderPlan_PositionsOKPD2 (id INTEGER, extNumber TEXT, code TEXT, name TIME);

-- Таблица: tenderPlan_printForm
DROP TABLE IF EXISTS tenderPlan_printForm;
CREATE TABLE tenderPlan_printForm (id INTEGER, url TEXT, signature TEXT);

-- Таблица: tenderPlan_specialPurchasePositions
DROP TABLE IF EXISTS tenderPlan_specialPurchasePositions;
CREATE TABLE tenderPlan_specialPurchasePositions (id INTEGER, type TEXT, positionNumber TEXT, extNumber TEXT, IKZ TEXT, publishYear INTEGER, IKU TEXT, kvr INTEGER, purchaseNumber TEXT);

-- Таблица: tenderPlan_specialPurchasePositionsfinanceInfo
DROP TABLE IF EXISTS tenderPlan_specialPurchasePositionsfinanceInfo;
CREATE TABLE tenderPlan_specialPurchasePositionsfinanceInfo (id INTEGER, total DOUBLE, currentYear DOUBLE, firstYear DOUBLE, secondYear DOUBLE, subsecYears DOUBLE);

-- Таблица: tenderPlan_specialPurchasePositionsKVR
DROP TABLE IF EXISTS tenderPlan_specialPurchasePositionsKVR;
CREATE TABLE tenderPlan_specialPurchasePositionsKVR (id INTEGER, kvr INTEGER, total DOUBLE, currentYear DOUBLE, firstYear DOUBLE, secondYear DOUBLE, subsecYears DOUBLE);

-- Таблица: tenderPlan_specialPurchasePositionsModification
DROP TABLE IF EXISTS tenderPlan_specialPurchasePositionsModification;
CREATE TABLE tenderPlan_specialPurchasePositionsModification (id INTEGER, changeReason INTEGER, decision BOOLEAN, description TEXT, modificationNumber INTEGER);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
